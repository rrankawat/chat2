<?php

// database connection
include('database_connection.php');

// session start
session_start();

$query = "
	SELECT * FROM login
	WHERE user_id != '".$_SESSION['user_id']."'
";

$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();

?>

<?php 
	foreach($result as $key => $row): 
		$status = '';
		$current_timestamp = strtotime(date('Y-m-d H:i:s') . '-10 second');
		$current_timestamp = date('Y-m-d H:i:s', $current_timestamp);
		$user_last_activity = fetch_user_last_activity($row['user_id'], $connect);
		if($user_last_activity > $current_timestamp) {
			$status = '<span class="label label-success">Online</span>';
		} else {
			$status = '<span class="label label-danger">Offline</span>';
		}
	?>
	<tr>
		<th><?php echo ($key+1); ?></th>
		<td><?php echo $row['username'].' '.count_unseen_message($row['user_id'], $_SESSION['user_id'], $connect).' '.fetch_is_type_status($row['user_id'], $connect); ?></td>
		<td><?php echo $status; ?></td>
		<td>
			<button type="button" class="btn btn-info btn-xs start_chat" data-touserid="<?php echo $row['user_id']; ?>" data-tousername="<?php echo $row['username']; ?>">Start Chat</button>
		</td>
    </tr>
<?php endforeach; ?>